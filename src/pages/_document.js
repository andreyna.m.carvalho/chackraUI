/* eslint-disable @next/next/no-title-in-document-head */
import Document, { Html, NextScript, Main, Head } from "next/document";

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en-US">
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Charis+SIL&family=Roboto:wght@400;500;700&display=swap"
            rel="stylesheet"
          />
          <title>Chackra UI</title>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
