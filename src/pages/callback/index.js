/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { Spinner } from "@chakra-ui/react";
import { useEffect } from "react";
import Header from "../../components/Header";

export default function CallBack() {
  const { data: session } = useSession();
  const router = useRouter();

  const loginButton = () => {
    if (!session) {
      router.replace("/");
    } else {
      router.replace("/users");
    }
  };

  const timeOut = setTimeout(loginButton, 5000);

  useEffect(() => {
    if (typeof window !== "undefined") {
      timeOut;
    }
  }, [router, session, timeOut]);

  return (
    <React.Fragment>
      <Header />
      <Spinner color="orange.700" />
    </React.Fragment>
  );
}
