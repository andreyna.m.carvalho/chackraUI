/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/no-unescaped-entities */
import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";
import Header from "../components/Header";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";

export default function SignIn() {
  const { data: session } = useSession();
  const router = useRouter();

  if (session) {
    router.replace("/users");
  }

  return (
    <Flex
      w="100vw"
      align="center"
      justify="center"
      display="flex"
      flexDirection="column"
    >
      <Header />
      <Box>
        <Text>Favor fazer login</Text>
      </Box>
    </Flex>
  );
}
