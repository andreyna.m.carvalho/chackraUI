/* eslint-disable import/no-anonymous-default-export */
export default (request, response) => {
  const users = [
    {
      id: 1,
      name: "Andreyna Carvalho",
      email: "andreyna.m.carvalho@gmail.com",
      linkedin: "https://www.linkedin.com/in/andreyna-carvalho-997273231/",
      github: "https://github.com/andreyna1808",
      phone: "(48) 9 9105-2198",
    },
  ];

  return response.json(users);
};
