/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/jsx-key */
import {
  Box,
  Button,
  Checkbox,
  Flex,
  Heading,
  Icon,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useBreakpointValue,
} from "@chakra-ui/react";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";
import { useState } from "react";

import { RiAddLine, RiPencilLine } from "react-icons/ri";
import Pagination from "../../components/pagination";
import Link from "next/link";
import axios from "axios";
import { useEffect } from "react";
import { UrlListUsers } from "../../utils/url";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";

export default function Users() {
  if (typeof window === "undefined") {
    return null;
  }
  
  const [user, setUser] = useState([]);
  const [page, setPage] = useState(1);
  const [pagination, setPagination] = useState({});
  const { data: session } = useSession();
  const router = useRouter();

  if (!session) {
    router.replace("/");
  }

  const isDesktop = useBreakpointValue({
    base: false,
    lg: true,
  });

  useEffect(() => {
    axios
      .get(`${UrlListUsers}`, { params: { page } })
      .then((res) => {
        const { pageEnd, pageStart, totalCount, users } = res.data;
        const getUser = users.map((user) => {
          return {
            id: user.id,
            name: user.name,
            email: user.email,
            createAt: new Date(user.createAt).toLocaleDateString("pt-BR", {
              day: "2-digit",
              month: "2-digit",
              year: "numeric",
            }),
          };
        });
        setUser(getUser);
        setPagination({ pageEnd, pageStart, totalCount });
      })
      .catch((err) => {
        console.log("Algo de errado não está certo no(a)", err); //colocar um toast
      });
  }, [page]);

  return (
    <Box>
      <Header />

      <Flex>
        <Sidebar />

        <Box
          mx={["2", "8"]}
          flex="1"
          borderRadius={["6", "8"]}
          bg="gray.800"
          p={["6", "8"]}
        >
          <Flex mb={["6", "8"]} justify="space-between" align="center">
            <Heading size="lg" fontWeight="normal">
              Usuários
            </Heading>

            <Link href="/users/create" passHref>
              <Button
                as="a"
                size="sm"
                fontSize="sm"
                colorScheme="orange"
                leftIcon={<Icon as={RiAddLine} />}
              >
                Criar novo
              </Button>
            </Link>
          </Flex>

          <Table colorScheme="whiteAlpha">
            <Thead>
              <Tr>
                <Th px={["4", "6"]} color="gray.100" width={["6", "8"]}>
                  <Checkbox colorScheme="orange" />
                </Th>
                <Th color="gray.300">Usuários</Th>
                {isDesktop && <Th color="gray.300">Data de Cadastro</Th>}
                <Th width={["6", "8"]}></Th>
              </Tr>
            </Thead>

            <Tbody>
              {user.map((user) => {
                return (
                  <Tr key={user.id}>
                    <Td px={["4", "6"]}>
                      <Checkbox colorScheme="orange" />
                    </Td>
                    <Td>
                      <Box>
                        <Text>{user.name}</Text>
                        <Text>{user.email}</Text>
                      </Box>
                    </Td>
                    {isDesktop && <Td>{user.createAt}</Td>}
                    <Td>
                      <Button
                        type="button"
                        as="a"
                        size="sm"
                        fontSize="sm"
                        bg="gray.700"
                        _hover={{ bg: "orange.700" }}
                        leftIcon={isDesktop && <Icon as={RiPencilLine} />}
                      >
                        {isDesktop ? "Editar" : <Icon as={RiPencilLine} />}
                      </Button>
                    </Td>
                  </Tr>
                );
              })}
            </Tbody>
          </Table>
          <Pagination
            pagination={pagination}
            onPageChange={setPage}
            currentPage={page}
          />
        </Box>
      </Flex>
    </Box>
  );
}
