import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  Input,
  SimpleGrid,
  VStack,
} from "@chakra-ui/react";
import Link from "next/link";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";

import { Formik } from "formik";
import * as yup from "yup";
import { InputControl } from "formik-chakra-ui";
import useForm from "../../services/hooks/useForm";
import axios from "axios";
import { UrlListUsers } from "../../utils/url";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";

const signInFormSchema = yup.object().shape({
  name: yup.string().required("Campo obrigatório"),
  email: yup.string().required("Campo obrigatório").email("Email inválido"),
  password: yup
    .string()
    .required("Campo obrigatório")
    .min(8, "Mínimo 8 dígitos"),
  password_confirmation: yup
    .string()
    .required("Campo obrigatório")
    .min(8, "Mínimo 8 dígitos")
    .oneOf([null, yup.ref("password")], "Senhas não correspondem"),
});

export default function CreateUser() {
  const { data: session } = useSession();
  const router = useRouter();

  if (!session) {
    router.replace("/");
  }

  const [form, onChange, clear] = useForm({
    name: "",
    email: "",
    password: "",
    password_confirmation: "",
  });

  const submitCreateUser = () => {
    axios
      .post(`${UrlListUsers}`, { user: { ...form, createAt: new Date() } })
      .then((res) => {
        clear();
      })
      .catch((err) => {
        alert("Erro", err);
      });
  };

  return (
    <Box>
      <Header />

      <Flex w="100%" my="6" mx="auto" px="6">
        <Sidebar />

        <Formik
          initialValues={{
            email: "",
            password: "",
            name: "",
            password_confirmation: "",
          }}
          validationSchema={signInFormSchema}
        >
          <Box flex="1" borderRadius={8} bg="gray.800" p={["6", "8"]}>
            <Heading size="lg" fontWeight="normal">
              Criar usuário
            </Heading>

            <Divider my="6" bg="gray.700" />

            <VStack spacing={["6", "8"]}>
              <SimpleGrid minChildWidth="240px" spacing={["6", "8"]} w="100%">
                <InputControl
                  name="name"
                  type="name"
                  value={form.name}
                  onChange={onChange}
                  inputProps={{
                    type: "text",
                    focusBorderColor: "orange.600",
                    placeholder: "Name",
                    bgColor: "gray.900",
                  }}
                />
                <InputControl
                  name="email"
                  value={form.email}
                  onChange={onChange}
                  inputProps={{
                    type: "email",
                    focusBorderColor: "orange.600",
                    placeholder: "Email",
                    bgColor: "gray.900",
                  }}
                />
              </SimpleGrid>
              <SimpleGrid minChildWidth="240px" spacing={["6", "8"]} w="100%">
                <InputControl
                  name="password"
                  value={form.password}
                  onChange={onChange}
                  inputProps={{
                    type: "password",
                    focusBorderColor: "orange.600",
                    placeholder: "Password",
                    bgColor: "gray.900",
                  }}
                />
                <InputControl
                  name="password_confirmation"
                  value={form.password_confirmation}
                  onChange={onChange}
                  inputProps={{
                    type: "password",
                    focusBorderColor: "orange.600",
                    placeholder: "Password Confirmation",
                    bgColor: "gray.900",
                  }}
                />
              </SimpleGrid>
            </VStack>
            <Flex mt="8" justify="flex-end">
              <HStack spacing="4">
                <Link href="/users" passHref>
                  <Button as="a" colorScheme="whiteAlpha">
                    Cancelar
                  </Button>
                </Link>
                <Button onClick={submitCreateUser} colorScheme="orange">
                  Salvar
                </Button>
              </HStack>
            </Flex>
          </Box>
        </Formik>
      </Flex>
    </Box>
  );
}
