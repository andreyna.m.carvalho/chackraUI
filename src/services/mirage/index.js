import { createServer, Factory, Model, ActiveModelSerializer } from "miragejs";
import { faker } from "@faker-js/faker";

export default function MakeServer() {
  const server = createServer({
    serializers: {
      application: ActiveModelSerializer,
    },

    models: {
      user: Model.extend({}),
    },

    factories: {
      user: Factory.extend({
        name(i) {
          return faker.internet.userName();
        },
        email() {
          return faker.internet.email().toLowerCase();
        },
        createAt() {
          return faker.date.recent(10);
        },
      }),
    },

    seeds(server) {
      server.createList("user", 12);
    },

    routes() {
      this.namespace = "api";
      this.timing = 750;

      this.get("/users", function (schema, request) {
        const { page = 1, per_page = 10 } = request.queryParams;

        const total = schema.all("user").length;

        const pageStart = ((Number(page) - 1) * Number(per_page) + 1);
        const pageEnd = pageStart + Number(per_page);

        const users = this.serialize(schema.all("user")).users.slice(
          pageStart,
          pageEnd
        );

        return {
          totalCount: total,
          pageStart,
          pageEnd,
          users,

        } //new Response(200, { "x-total-count": String(total) }, { users });
      });

      this.post("/users");

      this.namespace = "";
      this.passthrough();
    },
  });
  return server;
}
