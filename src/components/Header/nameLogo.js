import { Text } from "@chakra-ui/react";

export function NameLogo() {
  return (
    <Text
      color="orange.400"
      fontSize={['2xl', '3xl', '4xl']}
      fontWight="bold"
      letterSpacing="tight"
      w="64"
    >
      Dev
      <Text as="span" color="whiteAlpha.900" ml="1">
        Drica
      </Text>
    </Text>
  );
}
