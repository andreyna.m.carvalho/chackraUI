import { Avatar, Box, Flex, Text } from "@chakra-ui/react";
import React from "react";
import { signIn, signOut, useSession } from "next-auth/react";
import { FaGithub } from "react-icons/fa";

export function Profile({ showProfile }) {
  const { data: session } = useSession();

  return (
    <Flex
      cursor="pointer"
      onClick={!session ? () => signIn() : () => signOut()}
      align="center"
    >
      {!session ? (
        <Box display="flex" mr="4" alignItems="center">
          <FaGithub color="#FF8C00" />
          <Text px="2" _hover={{color: 'orange.700'}}>Sign in</Text>
        </Box>
      ) : showProfile ? (
        <Box mr="4" textAlign="right">
          <Text>{session.user.name}</Text>
          <Text color="gray.300" fontSize="small">
            {session.user.email}
          </Text>
        </Box>
      ) : (
        <Avatar size="md" name="Andreyna Carvalho" src={session.user.image} />
      )}
    </Flex>
  );
}
