import { Button, HStack, Icon } from "@chakra-ui/react";
import { RiNotificationLine, RiUserLine } from "react-icons/ri";

export function NotificationsNav() {
  return (
    <HStack
      spacing={['4', '6', '8']}
      mx={['4', '6', '8']}
      pr={['4', '6', '8']}
      py="1"
      color="gray.100"
      borderRightWidth={1}
      borderColor="gray.400"
    >
      <Button
        variant="ghost"
        _hover={{ variant: "ghost", color: "orange.700" }}
        aria-label="button notification"
      >
        <Icon as={RiNotificationLine} fontSize="20" />
      </Button>
      <Button
        variant="ghost"
        _hover={{ variant: "ghost", color: "orange.700" }}
        aria-label="button user personal"
      >
        <Icon as={RiUserLine} fontSize="20" />
      </Button>
    </HStack>
  );
}
