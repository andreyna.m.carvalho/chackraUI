import {
  Flex,
  Icon,
  IconButton,
  Text,
  useBreakpointValue,
} from "@chakra-ui/react";
import { Profile } from "./profile";
import { SearchBox } from "./searchBox";
import { NotificationsNav } from "./notificationsNav";
import { NameLogo } from "./nameLogo";
import { useSidebarDrawer } from "../../context/sidebarDrawerContext";
import { RiMenuLine } from "react-icons/ri";
import { useSession } from "next-auth/react";
import React from "react";

export default function Header() {
  const { onOpen } = useSidebarDrawer();
  const { data: session } = useSession();

  const isDesktop = useBreakpointValue({
    base: false,
    lg: true,
  });

  return (
    <Flex as="header" w="100%" h="20" mx="auto" mt="4" px="6" align="center">
      {!session ? (
        <Flex w="100%" display='flex' justifyContent='space-between'>
          <NameLogo />
          <Profile showProfile={isDesktop} />
        </Flex>
      ) : !isDesktop ? (
        <IconButton
          aria-label="Open navigation"
          icon={<Icon as={RiMenuLine} />}
          fontSize="24"
          variant="unstyled"
          onClick={onOpen}
          mr="2"
        />
      ) : (
        <React.Fragment>
          <NameLogo />
          {isDesktop && <SearchBox />}

          <Flex align="center" ml="auto">
            <NotificationsNav />
            <Profile showProfile={isDesktop} />
          </Flex>
        </React.Fragment>
      )}
    </Flex>
  );
}
