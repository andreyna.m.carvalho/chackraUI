/* eslint-disable react-hooks/exhaustive-deps */

import Link, { LinkProps } from 'next/link';
import { cloneElement, ReactElement } from 'react';
import { useRouter } from 'next/router';

export function ActiveLink({ children, ...rest}) {
    let isActive = false
    const { asPath } = useRouter()

    if(asPath === rest.href || asPath === rest.as){
        isActive = true
    } 

  return (
    <Link {...rest} >
        {cloneElement(children, {
            color: isActive ? 'orange.700' : 'gray.300'
        })}
    </Link>
  );
}
