import { Button } from "@chakra-ui/react";

export function PaginationItem({ isCurrent=false, number, onPageChange }) {
  if (isCurrent) {
    return (
      <Button
        size="sm"
        fontSize="xs"
        width="4"
        bg="orange.700"
        disabled
        _disabled={{ bg: "orange.900", cursor: "default" }}
        _hover={{ bg: "orange.900" }}
      >
        {number}
      </Button>
    );
  } else {
    return (
      <Button
        size="sm"
        fontSize="xs"
        width="4"
        bg="gray.700"
        _hover={{ bg: "orange.900" }}
        onClick={() => onPageChange(number)}
      >
        {number}
      </Button>
    );
  }
}
