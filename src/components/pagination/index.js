/* eslint-disable react/jsx-key */
import { Box, Button, Stack, Text } from "@chakra-ui/react";
import React from "react";
import { PaginationItem } from "./paginationItem";

// { pageEnd, pageStart, totalCount }

export default function Pagination({ pagination, onPageChange, currentPage }) {
  const { pageEnd, totalCount } = pagination;

  const lastPage = Math.floor(totalCount / pageEnd);

  function generatePagesArray(from, to) {
    return [...new Array(to - from)]
      .map((_, index) => {
        return from + index + 1;
      })
      .filter((page) => page > 0);
  }

  const previousPages =
    currentPage > 1 ? generatePagesArray(currentPage - 2, currentPage - 1) : [];

  const nextPages =
    currentPage < lastPage
      ? generatePagesArray(currentPage, Math.min(currentPage + 1, lastPage))
      : [];

  return (
    <Stack
      direction={["column", "row"]}
      spacing="6"
      mt="8"
      justify="space-between"
      align="center"
    >
      <Box>
        <strong>0</strong> - <strong>10</strong> de <strong>100</strong>
      </Box>
      <Stack direction="row" spacing="2">
        {currentPage > 2 && (
          <React.Fragment>
            <PaginationItem onPageChange={onPageChange} number={1} />
            {currentPage > 3 && <Text>...</Text>}
          </React.Fragment>
        )}

        {previousPages.length > 0 &&
          previousPages.map((page) => {
            return <PaginationItem onPageChange={onPageChange} key={page} number={page} />;
          })}

        <PaginationItem onPageChange={onPageChange} number={currentPage} isCurrent />

        {nextPages.length > 0 &&
          nextPages.map((page) => {
            return <PaginationItem onPageChange={onPageChange} key={page} number={page} />;
          })}

        {currentPage + 1 < lastPage && (
          <React.Fragment>
            {currentPage + 2 < lastPage && <Text>...</Text>}
            <PaginationItem onPageChange={onPageChange} number={lastPage} />
          </React.Fragment>
        )}
      </Stack>
    </Stack>
  );
}
