import { Box, Stack, Text } from "@chakra-ui/react";

export function NavSection({ title, children }) {
  return (
    <Box>
      <Text fontWeight="bold" color="gray.100" fontSize="small">
        {title}
      </Text>
      <Stack spacing="4" mt="8" align="stretch">
        {children}
      </Stack>
    </Box>
  );
}
