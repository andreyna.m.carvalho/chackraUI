import { Icon, Link as ChackraLink, Text } from "@chakra-ui/react";
import Link from "next/link";
import { ActiveLink } from "../activeLink";

export function NavLink({ icon, children, href, ...rest }) {
  return (
    <ActiveLink href={href} passHref>
      <ChackraLink display="flex" aling="center" {...rest}>
        <Icon as={icon} fontSize="20" />
        <Text fontWeight="medium" ml="4">
          {children}
        </Text>
      </ChackraLink>
    </ActiveLink>
  );
}
