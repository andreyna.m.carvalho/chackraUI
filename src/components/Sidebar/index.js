import {
  useBreakpointValue,
  Box,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
} from "@chakra-ui/react";
import { useState } from "react";
import { useSidebarDrawer } from "../../context/sidebarDrawerContext";
import SidebarNav from "./sidebarNav";

export default function Sidebar() {
  const { isOpen, onClose } = useSidebarDrawer()

  const isFloatSidebar = useBreakpointValue({
    base: true,
    lg: false,
  });

  if (isFloatSidebar) {
    return (
      <Drawer isOpen={isOpen} placement="left" onClose={onClose}>
        <DrawerOverlay>
          <DrawerContent bg='gray.800' p='4' >
            <DrawerCloseButton mt="6" />
            <DrawerHeader>Navegação</DrawerHeader>
            <DrawerBody>
              <SidebarNav />
            </DrawerBody>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
    );
  } else {
    return (
      <Box as="aside" w="64" mr="8">
        <SidebarNav />
      </Box>
    );
  }
}
