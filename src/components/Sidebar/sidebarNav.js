import { Box, Stack } from "@chakra-ui/react";
import {
  RiContactsLine,
  RiDashboardLine,
  RiGitMergeLine,
  RiInputMethodLine,
} from "react-icons/ri";
import { NavLink } from "./navLink";
import { NavSection } from "./navSection";

export default function SidebarNav() {
  return (
    <Stack spacing="12" align="flex-start">
      <NavSection title="GERAL">
        <NavLink href='/dashboard' icon={RiDashboardLine}>Dashboard</NavLink>
        <NavLink href='/users' icon={RiContactsLine}>Usuários</NavLink>
      </NavSection>

      <Box>
        <NavSection title="AUTOMOÇÃO">
          <NavLink href='/forms' icon={RiInputMethodLine}>Formulários</NavLink>
          <NavLink href='/automation' icon={RiGitMergeLine}>Automoção</NavLink>
        </NavSection>
      </Box>
    </Stack>
  );
}
